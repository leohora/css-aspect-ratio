# Propriedade CSS: aspect-ratio

A propriedade CSS aspect-ratio permite definir a proporção desejada entre largura e altura de um elemento. Isso significa que mesmo que o contêiner pai ou o tamanho da janela de visualização sejam alterados, o navegador ajustará as dimensões do elemento para manter a proporção largura/altura especificada.

Funciona muito bem com imagens, mas pode ser aplicado em outros elementos HTML. 😉

🔴 ATENÇÃO: Pelo menos um dos tamanhos do elemento precisa ser automático para que a proporção tenha algum efeito. Se nem a largura nem a altura forem um tamanho automático, a proporção fornecida não terá efeito.

## Redes Sociais
- [ ] [Instagram](https://www.instagram.com/leohoradev/)
- [ ] [YouTube](https://www.youtube.com/@LeoHoraDev)

## Contato
- [ ] leohoradev@gmail.com